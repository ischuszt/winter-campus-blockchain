# Fabric 

In order to start the network with fabric samples, you need to first start the network:

```
./network.sh up createChannel -ca -c mychannel -s couchdb
```

## Chaincode 

To deploy the chaincode:

```
./network.sh deployCC -ccn ex1 -ccp /home/cristi/tmp/winter-campus-blockchain/chaincode/ex1 -ccl javascript
```

or


```
./network.sh deployCC -ccn ex2 -ccp /home/cristi/tmp/winter-campus-blockchain/chaincode/ex2 -ccl javascript
```

## Client apps

Navigate to the corresponding dir, `npm install` and then `node app.js`.