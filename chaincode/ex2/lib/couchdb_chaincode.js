
const { Contract } = require('fabric-contract-api');

class Chaincode extends Contract {
  async InitLedger(ctx) {
    const assets = [
      {
        assetID: 'asset1',
        color: 'blue',
        size: 5,
        owner: 'Tom',
        appraisedValue: 100
      },
      {
        assetID: 'asset2',
        color: 'red',
        size: 5,
        owner: 'Brad',
        appraisedValue: 100
      }
    ]

    for (const asset of assets) {
      await this.CreateAsset(
        ctx,
        asset.assetID,
        asset.color,
        asset.size,
        asset.owner,
        asset.appraisedValue
      );
      console.info(`Asset ${asset.ID} initialized`);
    }
  }

  async CreateAsset(ctx, id, color, size, owner, appraisedValue) {
    const asset = {
      assetID: id,
      color: color,
      size: size,
      owner: owner,
      appraisedValue: appraisedValue,
    };

    // === Save asset to state ===
    await ctx.stub.putState(id, Buffer.from(JSON.stringify(asset)));
    let indexName = 'color~name';
    let colorNameIndexKey = await ctx.stub.createCompositeKey(indexName, [asset.color, asset.assetID]);

    //  Save index entry to state. Only the key name is needed, no need to store a duplicate copy of the marble.
    //  Note - passing a 'nil' value will effectively delete the key from state, therefore we pass null character as value
    await ctx.stub.putState(colorNameIndexKey, Buffer.from('\u0000'));
  }

  async ReadAsset(ctx, id) {
    const assetJSON = await ctx.stub.getState(id); // get the asset from chaincode state
    if (!assetJSON || assetJSON.length === 0) {
      throw new Error(`The asset ${id} does not exist`);
    }
    return assetJSON.toString();
  }


  // GetAllAssets returns all assets found in the world state.
  async GetAllAssets(ctx) {
    const allResults = [];
    // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
    const iterator = await ctx.stub.getStateByRange('', '');
    let result = await iterator.next();
    while (!result.done) {
      const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
      let record;
      try {
        record = JSON.parse(strValue);
      } catch (err) {
        console.log(err);
        record = strValue;
      }
      allResults.push({ Key: result.value.key, Record: record });
      result = await iterator.next();
    }
    return JSON.stringify(allResults);
  }

  async QueryByColor(ctx, color) {
    // Query the color~name index by color
    // This will execute a key range query on all keys starting with 'color'
    let coloredAssetResultsIterator = await ctx.stub.getStateByPartialCompositeKey('color~name', [color]);

    // Iterate through result set and for each asset found, return it in the list
    let responseRange = await coloredAssetResultsIterator.next();
    const results = [];
    while (!responseRange.done) {
      if (!responseRange || !responseRange.value || !responseRange.value.key) {
        return;
      }

      let objectType;
      let attributes;
      (
        { objectType, attributes } = await ctx.stub.splitCompositeKey(responseRange.value.key)
      );

      console.log(objectType);
      let returnedAssetName = attributes[1];

      // Now call the transfer function for the found asset.
      // Re-use the same function that is used to transfer individual assets
      results.push({ "name": returnedAssetName, "attrs": attributes });
      responseRange = await coloredAssetResultsIterator.next();
    }
    return JSON.stringify(results)
  }

}

module.exports = Chaincode;