
const {Contract} = require('fabric-contract-api');

class Chaincode extends Contract {
  async InitLedger(ctx) {
    const assets = [
      {
        assetID: 'asset1',
        color: 'blue',
        size: 5,
        owner: 'Tom',
        appraisedValue: 100
      },
      {
        assetID: 'asset2',
        color: 'red',
        size: 5,
        owner: 'Brad',
        appraisedValue: 100
      }
    ]

    for (const asset of assets) {
      await this.CreateAsset(
        ctx,
        asset.assetID,
        asset.color,
        asset.size,
        asset.owner,
        asset.appraisedValue
      );
      console.info(`Asset ${asset.ID} initialized`);
    }
  }

  async CreateAsset(ctx, id, color, size, owner, appraisedValue) {
    const asset = {
      assetID: id,
      color: color,
      size: size,
      owner: owner,
      appraisedValue: appraisedValue,
    };
    ctx.stub.putState(id, Buffer.from(JSON.stringify(asset)));
    return JSON.stringify(asset);
  }

  async ReadAsset(ctx, id) {
    const assetJSON = await ctx.stub.getState(id); // get the asset from chaincode state
    if (!assetJSON || assetJSON.length === 0) {
      throw new Error(`The asset ${id} does not exist`);
    }
    return assetJSON.toString();
  }


  // GetAllAssets returns all assets found in the world state.
  async GetAllAssets(ctx) {
    const allResults = [];
    // range query with empty string for startKey and endKey does an open-ended query of all assets in the chaincode namespace.
    const iterator = await ctx.stub.getStateByRange('', '');
    let result = await iterator.next();
    while (!result.done) {
      const strValue = Buffer.from(result.value.value.toString()).toString('utf8');
      let record;
      try {
        record = JSON.parse(strValue);
      } catch (err) {
        console.log(err);
        record = strValue;
      }
      allResults.push({ Key: result.value.key, Record: record });
      result = await iterator.next();
    }
    return JSON.stringify(allResults);
  }

}

module.exports = Chaincode;